import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    buildToolsVersion = "28.0.3"
    compileSdkVersion(27)
    defaultConfig {
        minSdkVersion(26)
        targetSdkVersion(27)
        applicationId = "co.makery.app"
        versionCode = 1
        versionName = "0.1.0"
        javaCompileOptions
            .annotationProcessorOptions
            .includeCompileClasspath = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        setSourceCompatibility("1.8")
        setTargetCompatibility("1.8")
    }

    lintOptions {
        lintOptions { warning("InvalidPackage") }
    }

    packagingOptions {
        exclude("META-INF/config-properties.adoc")
        exclude("META-INF/INDEX.LIST")
        exclude("META-INF/io.netty.versions.properties")
        exclude("META-INF/spring-configuration-metadata.json")
    }
}

dependencies {
    compileOnly("io.micronaut:micronaut-inject-java:$micronautVersion")
    implementation("io.micronaut:micronaut-http-client:$micronautVersion")
    implementation("io.micronaut:micronaut-runtime:$micronautVersion")
    implementation("org.apache.logging.log4j:log4j-api:$log4jVersion")
    implementation(kotlin("stdlib-jdk8", KotlinCompilerVersion.VERSION))
    kapt("io.micronaut:micronaut-inject-java:$micronautVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
