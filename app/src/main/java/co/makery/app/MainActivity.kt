package co.makery.app

import android.app.Activity
import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainActivity : Activity() {

    @Inject
    lateinit var greeter: Greeter

    @Inject
    lateinit var googleClient: GoogleClient

    private val disposeBag = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MakeryApp.ctx.inject(this)

        println(greeter.greet("Blob"))

        googleClient.index()
            .subscribe { response -> println(response) }
            .apply { disposeBag.add(this) }
    }
}
