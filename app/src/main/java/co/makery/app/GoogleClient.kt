package co.makery.app

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.reactivex.Single

@Client("https://www.google.com")
interface GoogleClient {

    @Get("/")
    fun index(): Single<String>
}
