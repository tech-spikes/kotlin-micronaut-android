package co.makery.app

import javax.inject.Singleton

@Singleton
class Greeter {

    fun greet(name: String): String = "Hello $name"
}
