package co.makery.app

import android.app.Application
import io.micronaut.context.ApplicationContext
import io.micronaut.context.env.Environment

class MakeryApp : Application() {

    companion object {
        lateinit var ctx: ApplicationContext
    }

    override fun onCreate() {
        super.onCreate()
        ctx = ApplicationContext.build(MakeryApp::class.java, Environment.ANDROID).start()
    }
}
