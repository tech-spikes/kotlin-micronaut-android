# Example Micronaut & Android integration

This project contains a Hello World Android application configured with Micronaut integration. It is based on the [official 'Android Support' guide](https://docs.micronaut.io/latest/guide/index.html#android).

There are examples for dependency injection & a Micronaut HTTP client looking very similar to Retrofit.

## Build & Run

You can build the application as a standard Android application with `./gradlew assembleDebug`.